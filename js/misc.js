window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
    document.getElementById("top").style.right = "2em";
  } else {
    document.getElementById("top").style.right = "";
  }
}

function scrollWindow() {
  var scrollingElement = document.scrollingElement || document.documentElement;
  scrollTo(scrollingElement, 0, 1000);
  return false;
}

function scrollTo(element, to, duration) {
  var start = element.scrollTop,
  change = to - start,
  increment = 20;

  var animateScroll = function(elapsedTime) {
    elapsedTime += increment;
    var position = easeInOut(elapsedTime, start, change, duration);
    element.scrollTop = position;
    if (elapsedTime < duration) {
      setTimeout(function() {
        animateScroll(elapsedTime);
      }, increment);
    }
  };
  animateScroll(0);
}

function easeInOut(currentTime, start, change, duration) {
  currentTime /= duration / 2;
  if (currentTime < 1) {
    return change / 2 * currentTime * currentTime + start;
  }
  currentTime -= 1;
  return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
}

var $html = document.getElementsByTagName("BODY")[0];

function showModal() {
  $html.style.overflow = 'hidden';
}

function hideModal() {
  $html.style.overflow = '';
  location.href='#close';
}

update();
function update() {
  var show = document.getElementsByClassName('lightbox');
  var close = document.getElementsByClassName('close-modal');

  for (var i = 0; i < show.length; i++) {
    show[i].addEventListener('click', showModal, false);
  }

  for (var i = 0; i < close.length; i++) {
    close[i].addEventListener('click', hideModal, false);
  }
}
