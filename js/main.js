function jump(elID) {
  toggleAll();
  var dest = document.getElementById(elID);
  var scrollingElement = document.scrollingElement || document.documentElement;
  scrollTo(scrollingElement, dest.offsetTop, 500);
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}

var layout = document.getElementById('layout'),
menu = document.getElementById('menu'),
menuLink = document.getElementById('nav-icon'),
content = document.getElementById('main');

function toggleClass(element, className) {
  var classes = element.className.split(/\s+/),
  length = classes.length,
  i = 0;

  for(; i < length; i++) {
    if (classes[i] === className) {
      classes.splice(i, 1);
      break;
    }
  }
  if (length === classes.length) {
    classes.push(className);
  }
  element.className = classes.join(' ');
}

function toggleAll() {
  var active = 'active';
  toggleClass(layout, active);
  toggleClass(menu, active);
  toggleClass(menuLink, active);
  menuLink.classList.toggle("change");
}

menuLink.onclick = function() {
  toggleAll();
};

content.onclick = function() {
  if (menu.className.indexOf('active') !== -1) {
    toggleAll();
  }
};

refresh();

function refresh() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var response = this.responseText.split("\n");
      var seconds = parseInt(response[0]);
      document.getElementById("days").innerHTML = Math.floor(seconds / (3600*24));
      document.getElementById("hours").innerHTML = Math.floor((seconds % (3600*24)) / 3600);
      document.getElementById("updated").innerHTML = response[5];

      load.animate(response[1] * 100);
      temp.animate(response[2] * 100);
      ram.animate(response[3]);
      hdd.animate(response[4]);
    }
  };
  xhttp.open("GET", "systemInfo.txt?" + Math.random(), true);
  xhttp.send();
}

var load = radialIndicator('#load', {
  barColor: {
    0: '#33CC33',
    60: '#FFFF00',
    100: '#FF0000'
  },
  barWidth : 3,
  percentage: true
});

var temp = radialIndicator('#temp', {
  barColor: {
    1500: '#0066FF',
    5000: '#FFFF00',
    8000: '#FF0000'
  },
  format: '##,#°C',
  barWidth : 3,
  minValue: 1500,
  maxValue: 8000,
  format: function (value) {
    return Math.floor(value / 100) + '°C';
  }
});

var ram = radialIndicator('#ram', {
  barColor: {
    0: '#33CC33',
    60: '#FFFF00',
    100: '#FF0000'
  },
  barWidth : 3,
  percentage: true
});

var hdd = radialIndicator('#hdd', {
  barColor: {
    0: '#33CC33',
    60: '#FFFF00',
    100: '#FF0000'
  },
  barWidth : 3,
  percentage: true
});
