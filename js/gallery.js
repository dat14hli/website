function jump(elID) {
  var dest = document.getElementById(elID);
  var scrollingElement = document.scrollingElement || document.documentElement;
  scrollTo(scrollingElement, dest.offsetTop, 500);
}

var spinner = document.getElementById('spinner');
var more = document.getElementById('more');

var loading;
var images = 70;
function loadMore() {
  spinner.style.display = "block";
  more.style.display = "none";

  var elems = [];
  var fragment = document.createDocumentFragment();
  var overlay = document.getElementById('overlay');

  loading = images;
  for ( var i = 0; i < 10; i++ ) {
    var elem = getItemElement(images);
    fragment.appendChild( elem );
    overlay.innerHTML += "<div class=\"lightbox-target\" id=\"img" + images + "\"><img src=\"pic/gallery/" + images + ".jpg\"/><button class=\"pure-button pure-button-primary close-modal\" style=\"font-size: 125%\" title=\"Close\">Close</button></div>";
    elems.push( elem );
    images--;
    if (images == 0) {
      break;
    }
  }
  loading -= images;

  grid.appendChild( fragment );
  masonry.appended( elems );
  masonry.layout();
  new AnimOnScroll( document.getElementById( 'grid' ), {
    minDuration : 0.4,
    maxDuration : 0.7,
    viewportFactor : 0.2
  } );
  update();
}

function getItemElement(i) {
  var elem = document.createElement('li');
  var link = document.createElement('a');

  var clas = document.createAttribute("class");
  clas.value = "lightbox";

  var href = document.createAttribute("href");
  href.value = "#img" + i;

  link.setAttributeNode(clas);
  link.setAttributeNode(href);

  var image = document.createElement('img');

  var src = document.createAttribute("src");
  src.value = "pic/gallery/" + i + ".jpg";

  var id = document.createAttribute("id");
  id.value = "img";

  var style = document.createAttribute("style");
  style.value = "width:100%";

  image.setAttributeNode(src);
  image.setAttributeNode(id);
  image.setAttributeNode(style);

  image.onload = function () {
    loading--;
    if (loading == 0){
      if (images == 0) {
        spinner.style.display = "none";
        more.style.display = "none";
      } else {
        spinner.style.display = "none";
        more.style.display = "block";
      }
    }
  }

  link.appendChild(image);
  elem.appendChild(link);

  return elem;
}
